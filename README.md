
# Loyalty Service System

This is only the skeleton for the project and contains only some of the basic functionality. I am using MongoDB for testing; we can replace it with MySQL when it's ready. 

---

## Overview

The Loyalty Service System is designed to provide a globally accessible distributed service for a café chain, introducing a 'friends and family' loyalty-card scheme. This system rewards customers with discounts on in-store purchases based on their transaction/purchase history, allowing up to four people to share a loyalty-card account.

## Features

- **User Management**: Register, update, and manage user accounts.
- **Transaction Logging**: Record every purchase transaction to track user spending.
- **Loyalty Accounts**: Create and manage shared loyalty accounts for families or friends.
- **Points System**: Earn points for every purchase, with points convertible into discounts.
- **Discount Calculation**: Automatically calculate available discounts based on the transaction history.

## Technology Stack

- **Backend**: Go (Gin framework)
- **Database**: MySQL (but currently using MongoDB...)
- **Deployment**: Docker for containerization and Docker Compose for multi-container setups

## Project Structure

~~~
/loyalty-service
    /internal
		/account
            service.go  // Account management logic
            model.go    // Account model definition
        /api
            handler.go  // HTTP handlers for the web server
            router.go   // Router setup
		/discount
            service.go  // Discount calculation logic
            model.go    // Discount model definition
        /user
            service.go  // User management logic
            model.go    // User model definition
        /transaction
            service.go  // Transaction processing logic
            model.go    // Transaction model definition
    /pkg
        /db
            database.go  // Database connection and initialization
    Dockerfile
    docker-compose.yml
    go.mod
	main.go              // Entry point for the API server
    README.md
~~~

## Getting Started

### Prerequisites

- Go (version 1.15 or later)
- MongoDB (version 4.4 or later)    // Will replace with MySQL
- Docker and Docker Compose (for deployment)

### Installation

1. **Clone the Repository**:
~~~
git clone https://github.com/yourusername/loyalty-service.git 
cd loyalty-service
~~~

2. **Set Up Environment Variables**:
~~~
MONGO_URI=mongodb://localhost:27017/loyaltyService
~~~

3. **Start MongoDB**

Ensure MongoDB is running on your system _or_ use Docker to run a MongoDB container.

4. **Run the Application**:
~~~
go run main.go
~~~

The service will start running on `http://localhost:8080`.

### Using Docker Compose

To simplify the setup, use Docker Compose to run the service along with MongoDB in containers:
~~~
docker-compose up --build
~~~

#### Access mongo shell through docker
Connect to the Mongo Shell with:
~~~
 docker exec -it loyalty_service_mongo_container /bin/bash
 mongosh -u root -p example --authenticationDatabase admin
~~~
You can now interact with the collections and documents throught the shell

## API Documentation

Endpoints include:

- POST `/users` - Register a new user
- POST `/loyalty-accounts` - Create a new loyalty account
- POST `/transactions` - Log a new transaction
- POST `/api/transactions/{transactionID}/addpoints` - Add points from transaction
- POST `/invitations/create` - Create an invitation token
- POST `/invitations/accept` - Accept an invitation to an account
- POST `/invitations/decline` - Decline an invitation to an account
- ...and more.

## Testing
We could run unit tests and integration tests using Go's built-in test tool? 

But for the moment I have set up a simple scenario to test the api functionality. This can be used as the basis for the python script.

Note:
- The user, account, transaction and invitation types only have the bare minimum for the moment
- You will need to change userIDs, transactionID, invitation tokens, etc to reflect the actual MongoDB objectIDs - use the mongo shell to view them.

### Create user1
~~~
curl -X POST http://localhost:8080/users \
     -H 'Content-Type: application/json' \
     -d '{"name": "John Doe", "email": "john.doe@example.com", "password": "password123"}'
~~~

### Create user2
~~~
curl -X POST http://localhost:8080/users \
     -H 'Content-Type: application/json' \
     -d '{"name": "Jane Doe", "email": "jane.doe@example.com", "password": "password123"}'
~~~

### Create an account and add user1 and user2
- Give them 100 points as a welcome gift
- At the moment for every 1 euro spent the user gets 20 points (equivalent to 20 cent)
- I added an array of discounts which could be used as coupons? - I'm ignoring it for now
~~~
curl -X POST http://localhost:8080/loyalty-accounts \
     -H "Content-Type: application/json" \
     -d '{"userIds": ["{user1ID}", "{user2ID}"], "points": 100, "discounts": ["10% off", "20% off"]}'
~~~

### Add a transaction
- User1  buys a coffee for 3.70e
- The account for user1 and user2 will receive 3.70x0.2 or 74 points/cents
~~~
curl -X POST http://localhost:8080/transactions \
     -H 'Content-Type: application/json' \
     -d '{"userId": "{user1ID}", "amount": 3.70, "category": "coffee", "timestamp": 1618033988746}'	 
~~~

### Add points to account given a transaction
- I put the add points functionality in the transaction service for the moment
~~~
curl -X POST http://localhost:8080/transactions/{transactionID}/addpoints
~~~

### Create two more users to test invitation functionality
~~~
curl -X POST http://localhost:8080/users \
     -H 'Content-Type: application/json' \
     -d '{"name": "James Joyce", "email": "james.joyce@example.com", "password": "ulysses"}'
~~~
and
~~~
curl -X POST http://localhost:8080/users \
     -H 'Content-Type: application/json' \
     -d '{"name": "Homer Simpson", "email": "homer.simpson@example.com", "password": "donuts"}'
~~~

### User1 creates an invitation for user3
~~~
curl -X POST "http://localhost:8080/invitations/create" \
     -H "Content-Type: application/json" \
     -d '{"email": "james.joyce@example.com", "inviterID": "{user1ID}", "accountID": "{user1AccountID}"}'
~~~

### User1 creates an invitation for user4
~~~
curl -X POST "http://localhost:8080/invitations/create" \
     -H "Content-Type: application/json" \
     -d '{"email": "homer.simpson@example.com", "inviterID": "{user1ID}", "accountID": "{user1AccountID}"}'
~~~

### User3 accepts the invitation
- Invitation token can viewed in the 'invitations' collection
- User3 is now added to user1s account
- The invitation status is updated to 'accepted'
~~~
curl -X POST "http://localhost:8080/invitations/accept" \
     -H "Content-Type: application/json" \
     -d '{"token": "unique_invitation_token", "email": "james.joyce@example.com"}'
~~~

### User4 declines the invitation
- User3 is now added to user1s account
- User4 is not added to user1s account
- The invitation status is updated to 'declined'
~~~
curl -X POST "http://localhost:8080/invitations/decline" \
     -H "Content-Type: application/json" \
     -d '{"token": "unique_invitation_token", "email": "homer.simpson@example.com"}'
~~~


