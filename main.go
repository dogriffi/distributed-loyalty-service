package main

import (
	"context"
	"log"
	"loyalty-service/internal/account"
	"loyalty-service/internal/api"
	"loyalty-service/internal/discount"
	"loyalty-service/internal/invitation"
	"loyalty-service/internal/transaction"
	"loyalty-service/internal/user"
	"loyalty-service/pkg/db"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	// Connect to MongoDB
	client, err := db.Connect(os.Getenv("MONGO_URI")) // MONGO_URI='mongodb://localhost:27017/myDatabase'
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}
	defer client.Disconnect(context.Background())

	// Select the specific database
	database := client.Database("loyaltyService")

	// Initialize services with the database
	userService := user.NewService(database)
	accountService := account.NewService(database)
	transactionService := transaction.NewService(database, accountService)
	discountService := discount.NewService(transactionService)
	invitationService := invitation.NewService(database, userService, accountService)

	// Set up Gin router and routes
	router := gin.Default()

	// Initialize the handler with the services
	handler := api.NewHandler(userService, transactionService, discountService, accountService, invitationService)

	// Setup routes using the handler
	handler.SetupRoutes(router)

	// Start the HTTP server
	if err := router.Run(":8080"); err != nil {
		log.Fatalf("Failed to run server: %v", err)
	}
}
