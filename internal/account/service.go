package account

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Service provides methods for account management
type Service struct {
	db *mongo.Collection
}

// NewService creates a new account service with a MongoDB collection
func NewService(db *mongo.Database) *Service {
	return &Service{
		db: db.Collection("accounts"),
	}
}

// CreateAccount adds a new loyalty group account to the database
func (s *Service) CreateAccount(ctx context.Context, account Account) (Account, error) {
	result, err := s.db.InsertOne(ctx, account)
	if err != nil {
		return Account{}, err
	}
	account.ID = result.InsertedID.(primitive.ObjectID)
	return account, nil
}

// GetAccount retrieves an account by its ID
func (s *Service) GetAccount(ctx context.Context, accountID string) (Account, error) {
	var account Account
	id, _ := primitive.ObjectIDFromHex(accountID)
	err := s.db.FindOne(ctx, bson.M{"_id": id}).Decode(&account)
	if err != nil {
		return Account{}, err
	}
	return account, nil
}

// AddPoints increments points for a user's account based on the transaction amount
func (s *Service) AddPoints(ctx context.Context, userID string, points int) error {
	// Convert the userID string to MongoDB's ObjectId type
	userObjID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		// Handle invalid userID format
		return err
	}

	// Adjust the filter to target the account document where the userIds array contains the userObjID
	filter := bson.M{"userIds": userObjID}
	update := bson.M{"$inc": bson.M{"points": points}}

	result, err := s.db.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	if result.MatchedCount == 0 {
		return errors.New("no account found for the user")
	}
	return nil
}

// SubtractPoints subtracts points from a user's loyalty account
func (s *Service) SubtractPoints(ctx context.Context, userID string, pointsToSubtract int) error {
	if pointsToSubtract <= 0 {
		return errors.New("points to subtract must be positive")
	}

	// Convert userID string to ObjectID
	userObjID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return errors.New("invalid userID format")
	}

	// Adjust the filter to target the account document where the userIds array contains the userObjID
	filter := bson.M{"userIds": userObjID}
	update := bson.M{"$inc": bson.M{"points": -pointsToSubtract}} // Ensure to subtract points

	result, err := s.db.UpdateOne(ctx, filter, update)
	if err != nil {
		return err // Handle potential MongoDB error
	}

	if result.ModifiedCount == 0 {
		return errors.New("account not found or points not updated")
	}

	return nil
}

// AddUserToAccount adds a user to an account by updating the account document in MongoDB
func (s *Service) AddUserToAccount(ctx context.Context, userID, accountID string) error {
	userObjectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return errors.New("invalid user ID format")
	}

	accountObjectID, err := primitive.ObjectIDFromHex(accountID)
	if err != nil {
		return errors.New("invalid account ID format")
	}

	filter := bson.M{"_id": accountObjectID}
	update := bson.M{
		"$addToSet": bson.M{"userIds": userObjectID},
	}

	// Perform the update operation
	result, err := s.db.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	// Check if the account document was successfully updated
	if result.ModifiedCount == 0 {
		return errors.New("failed to add user to account or user already a member")
	}

	return nil
}
