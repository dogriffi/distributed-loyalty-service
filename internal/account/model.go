package account

import "go.mongodb.org/mongo-driver/bson/primitive"

// Account represents a loyalty group account
type Account struct {
	ID        primitive.ObjectID   `bson:"_id,omitempty"`
	UserIDs   []primitive.ObjectID `bson:"userIds"`
	Points    int                  `bson:"points"`
	Discounts []string             `bson:"discounts"`
}
