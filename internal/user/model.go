package user

// User represents a user in the loyalty service system.
type User struct {
	ID       string `bson:"_id,omitempty"` // MongoDB unique ID
	Name     string `bson:"name"`          // User's full name
	Email    string `bson:"email"`         // User's email address
	Password string `bson:"password"`      // Hashed password (make sure to never store plaintext passwords)
}
