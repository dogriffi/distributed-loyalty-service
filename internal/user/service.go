package user

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
)

// Service provides methods to interact with user data.
type Service struct {
	db *mongo.Collection
}

// NewService creates a new user service.
func NewService(db *mongo.Database) *Service {
	return &Service{
		db: db.Collection("users"),
	}
}

// CreateUser creates a new user in the database.
func (s *Service) CreateUser(ctx context.Context, u User) error {
	// Hash the user's password before storing it
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)

	_, err = s.db.InsertOne(ctx, u)
	return err
}

// GetUserByID retrieves a user by their ID from the database.
func (s *Service) GetUserByID(ctx context.Context, userID string) (*User, error) {
	var user User
	filter := bson.M{"_id": userID}
	err := s.db.FindOne(ctx, filter).Decode(&user)
	if err != nil {
		return nil, err // Handle the error appropriately
	}
	return &user, nil
}

// GetUserByEmail retrieves a user by their email from the database.
func (s *Service) GetUserByEmail(ctx context.Context, email string) (*User, error) {
	var user User
	filter := bson.M{"email": email} // Use "email" field to match the user
	err := s.db.FindOne(ctx, filter).Decode(&user)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errors.New("user not found")
		}
		// Handle other potential errors (e.g., database connection issues)
		return nil, err
	}
	return &user, nil
}
