package invitation

import "time"

type Invitation struct {
	ID        string    `bson:"_id,omitempty"`
	Email     string    `bson:"email"`
	Token     string    `bson:"token"`
	ExpiresAt time.Time `bson:"expires_at"`
	InviterID string    `bson:"inviter_id"`
	AccountID string    `bson:"account_id"`
	Status    string    `bson:"status"` // "pending", "accepted", or "declined"
}
