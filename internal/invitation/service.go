package invitation

import (
	"context"
	"errors"
	"loyalty-service/internal/account"
	"loyalty-service/internal/user"
	"math/rand"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service struct {
	db         *mongo.Collection
	userSvc    *user.Service
	accountSvc *account.Service
}

func NewService(db *mongo.Database, userSvc *user.Service, accountSvc *account.Service) *Service {
	return &Service{
		db:         db.Collection("invitations"),
		userSvc:    userSvc,
		accountSvc: accountSvc,
	}
}

// GetInvitationByToken looks up an invitation by its token
func (s *Service) GetInvitationByToken(ctx context.Context, token string) (*Invitation, error) {
	var invitation Invitation

	// Ensure the token is for an invitation that hasn't expired yet
	filter := bson.M{
		"token": token,
		// "expiresAt": bson.M{
		// 	"$gte": time.Now(),
		// },
	}

	err := s.db.FindOne(ctx, filter).Decode(&invitation)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errors.New("invitation not found or has expired")
		}
		return nil, err
	}

	return &invitation, nil
}

func (s *Service) CreateInvitation(ctx context.Context, email, inviterID, accountID string) error {
	// Generate a unique token for the invitation
	token := generateToken()

	// Set expiration date for the invitation (e.g., 48 hours from now)
	expiresAt := time.Now().Add(48 * time.Hour)

	// Create the invitation in the database
	inv := Invitation{
		Email:     email,
		Token:     token,
		ExpiresAt: expiresAt,
		InviterID: inviterID,
		AccountID: accountID,
		Status:    "pending",
	}

	_, err := s.db.InsertOne(ctx, inv)
	if err != nil {
		return err
	}

	return nil
}

func generateToken() string {
	// Simple token generation
	const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	rand.Seed(time.Now().UnixNano())
	b := make([]byte, 16)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func (s *Service) AcceptInvitation(ctx context.Context, token, inviteeEmail string) error {
	// Find the invitation by token and ensure it's valid
	inv, err := s.GetInvitationByToken(ctx, token)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return errors.New("invitation not found")
		}
		return err
	}
	if inv.Email != inviteeEmail {
		return errors.New("invitation email does not match invitee's email")
	}

	// Update the invitation's status to "accepted"
	objID, err := primitive.ObjectIDFromHex(inv.ID) // Convert string to ObjectID
	if err != nil {
		return errors.New("invalid invitation ID")
	}
	filter := bson.M{"_id": objID}
	update := bson.M{
		"$set": bson.M{"status": "accepted"},
	}
	_, err = s.db.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	// Fetch the user by email
	invitee, err := s.userSvc.GetUserByEmail(ctx, inviteeEmail)
	if err != nil {
		// handle error, user not found or other errors
		return err
	}

	// Now, `invitee` is a user object, you can access its ID
	userID := invitee.ID

	// Add user to the account
	return s.accountSvc.AddUserToAccount(ctx, userID, inv.AccountID)
}

func (s *Service) DeclineInvitation(ctx context.Context, token, inviteeEmail string) error {
	// Find the invitation by token and ensure it's valid
	inv, err := s.GetInvitationByToken(ctx, token)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return errors.New("invitation not found")
		}
		return err
	}
	if inv.Email != inviteeEmail {
		return errors.New("invitation email does not match invitee's email")
	}

	// Update the invitation's status to "declined"
	objID, err := primitive.ObjectIDFromHex(inv.ID) // Convert string to ObjectID
	if err != nil {
		return errors.New("invalid invitation ID")
	}

	filter := bson.M{"_id": objID}
	update := bson.M{
		"$set": bson.M{"status": "declined"},
	}
	_, err = s.db.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}
