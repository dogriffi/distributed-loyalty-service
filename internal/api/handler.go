package api

import (
	"log"
	"net/http"

	"loyalty-service/internal/account"
	"loyalty-service/internal/discount"
	"loyalty-service/internal/invitation"
	"loyalty-service/internal/transaction"
	"loyalty-service/internal/user"

	"github.com/gin-gonic/gin"
)

// Handler struct centralizes dependencies for HTTP handlers.
type Handler struct {
	userService        *user.Service
	transactionService *transaction.Service
	discountService    *discount.Service
	accountService     *account.Service
	invitationService  *invitation.Service
}

// NewHandler is the constructor for Handler.
func NewHandler(userSvc *user.Service, transactionSvc *transaction.Service, discountSvc *discount.Service, accountSvc *account.Service, invitationSvc *invitation.Service) *Handler {
	return &Handler{
		userService:        userSvc,
		transactionService: transactionSvc,
		discountService:    discountSvc,
		accountService:     accountSvc,
		invitationService:  invitationSvc,
	}
}

// SetupRoutes defines all application's routes.
func (h *Handler) SetupRoutes(router *gin.Engine) {
	// User account management
	router.POST("/users", h.RegisterUser)  // Register a new user
	router.GET("/users/:id", h.GetUser)    // Retrieve user details
	router.PUT("/users/:id", h.UpdateUser) // Update user details

	// Managing loyalty-card accounts (Linking family and friends)
	router.POST("/loyalty-accounts", h.CreateLoyaltyAccount)        // Create a new loyalty account
	router.PUT("/loyalty-accounts/:id", h.AddUserToLoyaltyAccount)  // Add a user to an existing loyalty account
	router.GET("/loyalty-accounts/:id", h.GetLoyaltyAccountDetails) // Get details of a loyalty account

	// Transaction history
	router.POST("/transactions", h.CreateTransaction)            // Log a new transaction
	router.GET("/users/:id/transactions", h.GetUserTransactions) // Retrieve a user's transaction history
	router.POST("/transactions/:id/addpoints", h.AddPointsFromTransaction)
	router.POST("/transactions/:id/subtractpoints", h.AddPointsFromTransaction)
	router.POST("/transactions/process", h.ProcessTransaction)

	// Discounts
	router.GET("/users/:id/discounts", h.CalculateUserDiscount) // Calculate available discounts for a user
	router.POST("/discounts/apply", h.ApplyDiscount)            // Apply a discount to a user's purchase

	// Invitations
	router.POST("invitations/create", h.CreateInvitation)
	router.POST("/invitations/accept", h.AcceptInvitation)
	router.POST("/invitations/decline", h.DeclineInvitation)
}

// Register a new user
func (h *Handler) RegisterUser(c *gin.Context) {
	var newUser user.User

	// Parse the JSON request body into newUser
	if err := c.BindJSON(&newUser); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request data"})
		return
	}

	// Basic validation (you might want to expand this)
	if newUser.Name == "" || newUser.Email == "" || newUser.Password == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Missing required fields"})
		return
	}

	// Call your service's createUser function
	err := h.userService.CreateUser(c.Request.Context(), newUser)
	if err != nil {
		// Handle specific error cases here as needed
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create user"})
		return
	}

	// Return success message (or you might return the created user object, excluding sensitive fields like Password)
	c.JSON(http.StatusCreated, gin.H{"message": "User registered successfully"})
}

// Fetch transactions for a user.
func (h *Handler) GetUserTransactions(c *gin.Context) {
	// Placeholder implementation. Extract user ID from path, validate it, call transactionService to fetch transactions, return the result.
	userID := c.Param("id")
	c.JSON(http.StatusOK, gin.H{"userID": userID, "transactions": "TODO: Need to implement"})
}

// Calculate discounts for a user.
func (h *Handler) CalculateUserDiscount(c *gin.Context) {
	// Placeholder implementation. Extract user ID, call discountService to calculate discounts, return the result.
	userID := c.Param("id")
	c.JSON(http.StatusOK, gin.H{"userID": userID, "discount": "TODO: Need to implement"})
}

// Logs a new transaction to a user's account.
func (h *Handler) LogTransaction(c *gin.Context) {
	// Implementation goes here.
	c.JSON(http.StatusCreated, gin.H{"message": "TODO: Need to implement"})
}

// Apply a discount to a user's loyalty account.
func (h *Handler) ApplyDiscount(c *gin.Context) {
	// Implementation goes here.
	c.JSON(http.StatusCreated, gin.H{"message": "TODO: Need to implement"})
}

// GetUser handles fetching user details by ID.
func (h *Handler) GetUser(c *gin.Context) {
	// Extract the user ID from the URL path
	userID := c.Param("id")

	// Use the userService to fetch the user by their ID
	user, err := h.userService.GetUserByID(c, userID)
	if err != nil {
		// If the user is not found or there's another error, return an appropriate response
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}

	// If the user is found, return their details
	c.JSON(http.StatusOK, user)
}

// Update user details
func (h *Handler) UpdateUser(c *gin.Context) {
	// Implementation goes here.
	c.JSON(http.StatusCreated, gin.H{"message": "TODO: Need to implement"})
}

// Add a user to an existing loyalty account
func (h *Handler) AddUserToLoyaltyAccount(c *gin.Context) {
	// Implementation goes here.
	c.JSON(http.StatusCreated, gin.H{"message": "TODO: Need to implement"})
}

// Create a new loyalty account
func (h *Handler) CreateLoyaltyAccount(c *gin.Context) {
	var acc account.Account
	if err := c.ShouldBindJSON(&acc); err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}
	createdAccount, err := h.accountService.CreateAccount(c.Request.Context(), acc)
	if err != nil {
		c.JSON(500, gin.H{"error": "Failed to create account"})
		return
	}
	c.JSON(201, createdAccount)
}

// Get details of a loyalty account
func (h *Handler) GetLoyaltyAccountDetails(c *gin.Context) {
	accountID := c.Param("id")
	acc, err := h.accountService.GetAccount(c.Request.Context(), accountID)
	if err != nil {
		c.JSON(404, gin.H{"error": "Account not found"})
		return
	}
	c.JSON(200, acc)
}

// Create a transaction
func (h *Handler) CreateTransaction(c *gin.Context) {
	var t transaction.Transaction

	// Attempt to bind the incoming JSON payload to the transaction struct
	if err := c.ShouldBindJSON(&t); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request data"})
		return
	}

	// Now, call the CreateTransaction method to save the transaction
	if err := h.transactionService.CreateTransaction(c.Request.Context(), t); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create transaction"})
		return
	}

	// If successful, respond with the created transaction details or a success message
	c.JSON(http.StatusCreated, t) // Or gin.H{"message": "Transaction created successfully"}
}

// Add points to loyalty account given a transaction id
func (h *Handler) AddPointsFromTransaction(c *gin.Context) {
	transactionID := c.Param("id") // Extract the transaction ID from the URL path

	// Call AddPointsForTransaction with the context and transactionID
	if err := h.transactionService.AddPointsForTransaction(c.Request.Context(), transactionID); err != nil {
		// Error handling: Transaction not found, database errors, etc.
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Success response
	c.JSON(http.StatusOK, gin.H{"message": "Points added successfully"})
}

func (h *Handler) SubtractPointsFromTransaction(c *gin.Context) {
	transactionID := c.Param("id") // Extract the transaction ID from the URL path

	// Call SubtractPointsForTransaction with the context and transactionID
	if err := h.transactionService.SubtractPointsForTransaction(c.Request.Context(), transactionID); err != nil {
		// Error handling: Transaction not found, database errors, etc.
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Success response
	c.JSON(http.StatusOK, gin.H{"message": "Points added successfully"})
}

// Process a new transaction and either add points or use points based on the transaction details.
func (h *Handler) ProcessTransaction(c *gin.Context) {
	var trans transaction.Transaction

	if err := c.ShouldBindJSON(&trans); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request data"})
		return
	}

	if err := h.transactionService.ProcessTransaction(c.Request.Context(), trans); err != nil {
		log.Printf("Error processing transaction: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to process transaction", "detail": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "Transaction processed successfully"})
}

func (h *Handler) CreateInvitation(c *gin.Context) {
	var req struct {
		Email     string `json:"email"`     // Email of the person being invited
		InviterID string `json:"inviterID"` // ID of the person sending the invitation
		AccountID string `json:"accountID"` // ID of the account/group to which the invitee is being invited
	}

	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request data"})
		return
	}

	// `CreateInvitation` equires the email of the invitee, the inviterID, and the accountID.
	err := h.invitationService.CreateInvitation(c.Request.Context(), req.Email, req.InviterID, req.AccountID)
	if err != nil {
		// Handle errors as appropriate for your application
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create invitation"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "Invitation created successfully"})
}

func (h *Handler) AcceptInvitation(c *gin.Context) {
	var req struct {
		Token        string `json:"token"`
		InviteeEmail string `json:"email"`
	}
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request data"})
		return
	}

	err := h.invitationService.AcceptInvitation(c.Request.Context(), req.Token, req.InviteeEmail)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Invitation accepted successfully"})
}

func (h *Handler) DeclineInvitation(c *gin.Context) {
	var req struct {
		Token        string `json:"token"`
		InviteeEmail string `json:"email"`
	}
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request data"})
		return
	}

	err := h.invitationService.DeclineInvitation(c.Request.Context(), req.Token, req.InviteeEmail)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Invitation declined successfully"})
}
