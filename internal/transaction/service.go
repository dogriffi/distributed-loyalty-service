package transaction

import (
	"context"
	"log"
	"loyalty-service/internal/account"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Service provides methods to interact with transaction data.
type Service struct {
	db         *mongo.Collection
	accountSvc *account.Service
}

// NewService creates a new transaction service.
func NewService(db *mongo.Database, accountSvc *account.Service) *Service {
	return &Service{
		db:         db.Collection("transactions"),
		accountSvc: accountSvc,
	}
}

// CreateTransaction saves a transaction to the database
func (s *Service) CreateTransaction(ctx context.Context, t Transaction) error {
	t.Timestamp = time.Now().Unix() // Set current UNIX timestamp if not set
	_, err := s.db.InsertOne(ctx, t)
	return err
}

func (s *Service) ProcessTransaction(ctx context.Context, transaction Transaction) error {
	session, err := s.db.Database().Client().StartSession()
	if err != nil {
		log.Printf("Failed to start session: %v", err)
		return err
	}
	defer session.EndSession(ctx)

	err = mongo.WithSession(ctx, session, func(sc mongo.SessionContext) error {
		if err := session.StartTransaction(); err != nil {
			log.Printf("Failed to start transaction: %v", err)
			return err
		}

		transaction.Timestamp = time.Now().Unix()
		_, err = s.db.InsertOne(sc, transaction)
		if err != nil {
			session.AbortTransaction(sc)
			log.Printf("Failed to insert transaction: %v", err)
			return err
		}

		if transaction.UsePoints {
			err = s.accountSvc.SubtractPoints(sc, transaction.UserID, int(transaction.Amount))
		} else {
			pointsToAdd := int(transaction.Amount * 20)
			err = s.accountSvc.AddPoints(sc, transaction.UserID, pointsToAdd)
		}

		if err != nil {
			session.AbortTransaction(sc)
			log.Printf("Failed to update points: %v", err)
			return err
		}

		if commitErr := session.CommitTransaction(sc); commitErr != nil {
			log.Printf("Failed to commit transaction: %v", commitErr)
			return commitErr
		}

		return nil
	})

	if err != nil {
		log.Printf("Transaction processing failed: %v", err)
	}
	return err
}

// GetTransactionsByUserID retrieves transactions for a specific user from the database
func (s *Service) GetTransactionsByUserID(ctx context.Context, userID string) ([]Transaction, error) {
	var transactions []Transaction
	filter := bson.M{"user_id": userID}
	cursor, err := s.db.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var t Transaction
		if err := cursor.Decode(&t); err != nil {
			return nil, err
		}
		transactions = append(transactions, t)
	}
	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return transactions, nil
}

// New method to calculate and add points to user's loyalty account
func (s *Service) AddPointsForTransaction(ctx context.Context, transactionID string) error {
	// Convert string ID to MongoDB's ObjectID
	objID, err := primitive.ObjectIDFromHex(transactionID)
	if err != nil {
		// Handle invalid ID format
		return err
	}

	var trans Transaction
	// Use the ObjectID for the query
	if err := s.db.FindOne(ctx, bson.M{"_id": objID}).Decode(&trans); err != nil {
		// Transaction not found or other database errors
		return err
	}

	// Calculate points: 20 points for every Euro spent
	pointsToAdd := int(trans.Amount * 20)

	// Assuming the account service has a method AddPoints accepting userID and points
	return s.accountSvc.AddPoints(ctx, trans.UserID, pointsToAdd)
}

func (s *Service) SubtractPointsForTransaction(ctx context.Context, transactionID string) error {
	// Convert string ID to MongoDB's ObjectID
	objID, err := primitive.ObjectIDFromHex(transactionID)
	if err != nil {
		// Handle invalid ID format
		return err
	}

	var trans Transaction
	// Use the ObjectID for the query
	if err := s.db.FindOne(ctx, bson.M{"_id": objID}).Decode(&trans); err != nil {
		// Transaction not found or other database errors
		return err
	}

	// Assuming you subtract a fixed amount of points for each transaction
	// For simplicity, let's subtract 10 points for each Euro spent as an example
	pointsToSubtract := int(trans.Amount * 10)

	// Assuming the account service has a method SubtractPoints accepting userID and points
	return s.accountSvc.SubtractPoints(ctx, trans.UserID, pointsToSubtract)
}
