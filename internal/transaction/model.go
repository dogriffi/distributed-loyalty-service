package transaction

// Transaction represents a transaction in the loyalty service system.
type Transaction struct {
	ID        string  `bson:"_id,omitempty"`                // MongoDB unique ID
	UserID    string  `bson:"user_id"`                      // ID of the user who made the transaction
	Amount    float64 `bson:"amount"`                       // Transaction amount
	Category  string  `json:"category" bson:"category"`     // For example, "coffee".
	Timestamp int64   `json:"timestamp" bson:"timestamp"`   // Correctly using int64 for UNIX timestamp
	UsePoints bool    `json:"use_points" bson:"use_points"` // Whether to use points for this transaction
}
