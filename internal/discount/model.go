package discount

// Discount represents a discount applied to a user's transaction.
type Discount struct {
	ID          string  `json:"id" bson:"_id,omitempty"`        // MongoDB unique ID
	Description string  `json:"description" bson:"description"` // Description of the discount
	Percentage  float64 `json:"percentage" bson:"percentage"`   // Discount percentage
	Value       float64 `json:"value" bson:"value"`             // Value of the discount, add this line
}
