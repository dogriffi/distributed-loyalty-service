package discount

import (
	"context"
	"loyalty-service/internal/transaction"
)

// Service provides methods to interact with discount data.
type Service struct {
	transactionService *transaction.Service
}

// NewService creates a new discount service.
func NewService(transactionService *transaction.Service) *Service {
	return &Service{
		transactionService: transactionService,
	}
}

// CalculateDiscount calculates the discount for a user based on their transaction history.
// Assuming each coffee purchase is logged as a separate transaction
// and you're not focusing on the amount spent but the number of coffees bought.
func (s *Service) CalculateDiscount(ctx context.Context, userID string) (Discount, error) {
	transactions, err := s.transactionService.GetTransactionsByUserID(ctx, userID)
	if err != nil {
		return Discount{}, err
	}

	// Count the number of coffee purchases
	var coffeePurchases int
	for _, t := range transactions {
		if t.Category == "coffee" {
			coffeePurchases++
		}
	}

	// Calculate the number of free coffees earned (1 free coffee for every 5 bought)
	freeCoffees := coffeePurchases / 5

	// Assuming the average price of a coffee to determine the discount value
	averageCoffeePrice := 3.0 // This could be calculated based on transaction data
	discountValue := float64(freeCoffees) * averageCoffeePrice

	return Discount{
		Description: "Free Coffee Discount",
		Value:       discountValue, // The total value of the free coffees earned
		// Make sure to adjust the Discount struct if needed to include a 'Value' field
	}, nil
}
